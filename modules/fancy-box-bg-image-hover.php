<div class="fancy-box-img-on-hover" onclick="window.location.href = <?= esc_attr($cta_link_url); ?>;">
    <div class="bg-image" style=""></div>
    <div class="fancy-box-inner">
        <div class="fancy-box-info">
            <h6 class="small-title">TODAY'S PROJECT</h6>
            <h6 class="duration">5 MIN READ</h6>
        </div>
        <div class="fancy-box-content">
            <h3>PROJECT TITLE</h3>
            <p>Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Maecenas sed diam eget risus varius blandit sit amet non.</p>
            <a href="#" class="btn-medium btn--black">READ</a>
        </div>
    </div>
</div>

