<?php
$landing_hero_image = types_render_field("landing-top-section-image", array('output'=>'raw','item'=>get_the_ID()));
$landing_hero_title = types_render_field("landing-top-section-title", array('item'=>get_the_ID()));   
$landing_hero_cta_text = types_render_field("landing-top-section-cta-button", array('item'=>get_the_ID()));  
$landing_buttons = toolset_get_related_posts( 
    get_the_ID(),
   'landing-ts-button', 
   array( 
       'query_by_role' => 'parent', 
       'return' => 'post_object' 
       ) 
   );  
  
?>

<div class="landing-top-section">
    <div class="container">
        <div class="landing-top-section-inner">
            <div class="bg-image" style="background-image: url('<?php echo $landing_hero_image; ?>');"></div>
            <h1 class="landing-top-section-title page-title"><?php echo $landing_hero_title; ?></h1> <!-- Εδώ ίσως θα πρέπει να μπεί h1 για λόγους SEO, έχω βάλει h3 επειδή στο design έχει style απο h3 -->
            <div class="cta-links">
            <?php                
                    $i=0;             
                    foreach ($landing_buttons as $landing_button) 
                    { 
                    if ($i < 2){
                       $button_text = types_render_field("landing-top-section-button-text", array('item'=>$landing_button->ID, 'output'=>'raw'));                       
                       $button_url = types_render_field("landing-top-section-button-link", array('item'=>$landing_button->ID, 'output'=>'raw'));
                       $button_url_target = types_render_field( 'landing-top-section-button-target', array('item'=>$landing_button->ID, 'output'=>'raw'));                     
                        ?> 
                         <a href="<?php echo $button_url; ?>" class="btn-large btn--transparent btn-light hover-red" target="<?php echo  $button_url_target; ?>"><?php echo $button_text; ?></a>                      
                      <?php  } 
                         $i++;
                     } ?>          
            </div>
        </div>
    </div>
    <a href="#primary" class="cta-button">
        <span class="base-text"><?php echo $landing_hero_cta_text; ?></span>
        <svg id="arrow-right" viewBox="0 0 62 33">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g id="FD.1.1.-Home-1920" transform="translate(-436.000000, -1746.000000)" stroke="#101010" stroke-width="2.4576">
                    <g id="Group" transform="translate(336.000000, 1747.000000)">
                        <path d="M144.334793,33.6594094 L145,34.322492 L134.933205,44.3571429 L134.267998,43.6940602 L144.334793,33.6594094 Z M116.665207,33.6594094 L126.732002,43.6940602 L126.066795,44.3571429 L116,34.322492 L116.665207,33.6594094 Z M130.885232,-13.6428571 L130.885232,29.9648662 L129.944488,29.9648662 L129.944488,-13.6428571 L130.885232,-13.6428571 Z" id="Combined-Shape-Copy-2" transform="translate(130.500000, 15.357143) scale(-1, 1) rotate(90.000000) translate(-130.500000, -15.357143) "></path>
                    </g>
                </g>
            </g>
        </svg>
    </a>
</div>