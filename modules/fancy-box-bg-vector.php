<div class="fancy-box-bg-vector bg-lightgrey red-hover" onclick="window.location.href = <?= esc_attr($cta_link_url); ?>;">
    <div class="bg-svg">
        <img src="">
    </div>
    <div class="cta-title">
        <h4>EXPLORE EUROPA PRODUCTS</h4>
    </div>
    <div class="cta-description">
        <div>OUR PRODUCTS</div>
        <div>
        <svg id="arrow-right" viewBox="0 0 62 33">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g id="FD.1.1.-Home-1920" transform="translate(-436.000000, -1746.000000)" stroke="#101010" stroke-width="2.4576">
                    <g id="Group" transform="translate(336.000000, 1747.000000)">
                        <path d="M144.334793,33.6594094 L145,34.322492 L134.933205,44.3571429 L134.267998,43.6940602 L144.334793,33.6594094 Z M116.665207,33.6594094 L126.732002,43.6940602 L126.066795,44.3571429 L116,34.322492 L116.665207,33.6594094 Z M130.885232,-13.6428571 L130.885232,29.9648662 L129.944488,29.9648662 L129.944488,-13.6428571 L130.885232,-13.6428571 Z" id="Combined-Shape-Copy-2" transform="translate(130.500000, 15.357143) scale(-1, 1) rotate(90.000000) translate(-130.500000, -15.357143) "></path>
                    </g>
                </g>
            </g>
        </svg>
        </div>
    </div>
</div>

