<div class="fancy-box-stories" onclick="window.location.href =<?= esc_attr($cta_link_url) ?>;">
    <div class="bg-color"></div>
    <div class="bg-image" style="background-image: url('<?= content_url() . '/uploads/home-hero-slider-image.jpg'; ?>')"></div>
    <div class="fancy-box-stories-inner">
        <div class="story-info">
            <h6 class="type">STORY</h6>
            <h6 class="duration">5 MIN READ</h6>
        </div>
        <div class="story-description">
            <h4>SEM PARTURIENT</h4>
            <p>
                Nulla vitae elit libero, a pharetra augue. Donec sed odio dui. Sed posuere consectetur est at lobortis.
            </p>
            <a href="#" class="btn-small btn--black">READ</a>
        </div>
    </div>
</div>
