
<div class="fancy-box-small-image" onclick="window.location.href = <?= esc_attr($cta_link_url); ?>;">
    <h6 class="small-title">01</h6>
    <h4>SPECIFICATION TEAM</h4>
    <img src="<?= get_template_directory_uri() . '/placeholder.jpg'; ?>" alt="">
    <p class="large-text">
        Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Maecenas sed diam eget risus varius blandit sit amet non magna. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit.
    </p>
    <div class="btn-wrapper">
        <div class="btn-small btn--black">SEE MORE</div>
    </div>
</div>
