<div id="home-hero-section">
    <div class="row">
        <div class="col-12 col-xl-6">
            <div class="home-hero-slider" data-custom-cursor="read-more">            
                <?php 
                    $home_slides = toolset_get_related_posts( 
                     get_the_ID(),
                    'home-page-slider', 
                    array( 
                        'query_by_role' => 'parent', 
                        'return' => 'post_object' 
                        ) 
                    );  
                    $i=0;              
                    foreach ($home_slides as $home_slide) 
                    { 
                    if ($i < 3){
                       $slide_title = types_render_field("slide-title", array('item'=>$home_slide->ID, 'output'=>'raw'));
                       $slide_description = types_render_field( 'slide-text', array('item'=>$home_slide->ID));
                       $slide_url = types_render_field("slide-link", array('item'=>$home_slide->ID, 'output'=>'raw'));
                       $slide_bg_image = types_render_field( 'slide-image', array('item'=>$home_slide->ID, 'output'=>'raw'));   
                       $slide_date = types_render_field("slide-date", array('item'=>$home_slide->ID, 'output'=>'raw'));  
                       $slide_date_text = types_render_field("slide-date-text", array('item'=>$home_slide->ID, 'output'=>'raw'));  
                       $slide_btn_text = types_render_field("slide-button-text", array('item'=>$home_slide->ID, 'output'=>'raw'));                
                        ?>  
                <div class="slide"  onclick="window.location.href='<?php echo  $slide_ur;  ?>'">
                    <div class="slide-background">
                        <img src="<?php echo  $slide_bg_image;  ?>" alt="">
                    </div>
                    <div class="slide-date-info"><?php echo  $slide_date_text;  ?></div>
                    <div class="slide-text">
                        <div class="slide-title"><?php echo   $slide_title;  ?></div>
                        <div class="slide-description base-text">
                        <?php echo   $slide_description;  ?>
                        </div>
                    </div>
                </div>
                <?php  } 
                         $i++;
                     } ?> 
                <!-- <div class="slide">
                    <div class="slide-background">
                        <img src="<?= content_url() . '/themes/wp-europa/placeholder.jpg'; ?>" alt="">
                    </div>
                    <div class="slide-date-info">24-12<br>2027</div>
                    <div class="slide-text">
                        <div class="slide-title">BAU<br>2024</div>
                        <div class="slide-description base-text">
                            TFusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Aenean lacinia bibendum nulla sed consectetur. Donec id elit non mi porta gravida at eget metus. Donec sed odio dui.
                        </div>
                    </div>
                </div> -->
            </div>
        </div>
        <div class="col-12 col-xl-6">
            <div class="row flex-column">
                <div class="home-hero-boxes">
                <?php 
                    $teaser_posts = toolset_get_related_posts( 
                     get_the_ID(),
                    'landing-teaser', 
                    array( 
                        'query_by_role' => 'parent', 
                        'return' => 'post_object' 
                        ) 
                    );  
                    $i=0;              
                    foreach ($teaser_posts as $teaser_post) 
                    { 
                    if ($i < 3){
                       $teaser_title = types_render_field("teaser-title", array('item'=>$teaser_post->ID, 'output'=>'html'));
                       $description = types_render_field( 'teaser-text', array('item'=>$teaser_post->ID));
                       $teaser_url = types_render_field("teaser-link", array('item'=>$teaser_post->ID, 'output'=>'raw'));
                       $teaser_bg_image = types_render_field( 'teaser-image', array('item'=>$teaser_post->ID, 'output'=>'raw'));                     
                        ?>  
                        <div class="home-hero-box">
                            <div class="bg-image" style="background-image: url('<?php  echo $teaser_bg_image; ?>');"></div>
                            <div class="home-hero-box-content">
                                <h5><?php echo $teaser_title ?></h5>
                                <div class="home-hero-box-description base-text">
                                <?php echo $description; ?>
                                </div>
                                <a href="<?php echo $teaser_url; ?>" class="btn-small btn--black"><?php _e('SEE MORE', 'wp-europa')?></a>
                            </div>
                       </div>
                     
                     <?php  } 
                         $i++;
                     } ?>                 
                </div>
                <div class="our-industrial-applications">
                <?php 
                    $industrial_posts = toolset_get_related_posts( 
                     get_the_ID(),
                    'industria-teaser', 
                    array( 
                        'query_by_role' => 'parent', 
                        'return' => 'post_object' 
                        ) 
                    );  
                    $n=0;              
                    foreach ($industrial_posts as $industrial_post) 
                    { 
                    if ($n < 1){
                       $industrial_title = types_render_field("industrial-teaser-title", array('item'=>$industrial_post->ID, 'output'=>'html'));
                       $industrial_description = types_render_field( 'industrial-teaser-text', array('item'=>$industrial_post->ID));
                       $industrial_url = types_render_field("industrial-teaser-link", array('item'=>$industrial_post->ID, 'output'=>'raw'));
                       $industrial_image = types_render_field( 'industrial-image', array('item'=>$industrial_post->ID, 'output'=>'raw'));                     
                        ?> 
                        <div class="our-industrial-applications-content">
                        <img src="<?php  echo $industrial_image; ?>" alt="">
                        <h5><?php echo $industrial_title ?></h5>
                        <div class="description base-text">
                            <?php echo $industrial_description; ?>
                        </div>
                        <a href="<?php echo $industrial_url; ?>" class="btn-small btn--black"><?php _e('SEE MORE', 'wp-europa')?></a>
                    </div>          
                     <?php  } 
                         $n++;
                     } ?>
                </div>
            </div>
        </div>
    </div>
</div>