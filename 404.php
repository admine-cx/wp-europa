<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package WP_Europa
 */

get_header(); ?>
<div class="container">
			<div class="row">
				<section id="primary" class="content-area col-sm-12">
					<div id="main" class="site-main" role="main">

						<section class="error-404 not-found">		

							<div class="page-content">
							<h1 class="page-title"><?php esc_html_e( 'Oops! That page can&rsquo;t be found.', 'wp-europa' ); ?></h1>
								<p><?php esc_html_e( 'It looks like nothing was found at this location. Maybe try one of the links below or a search?', 'wp-europa' ); ?></p>

								<?php
									get_search_form();
								?>

							</div><!-- .page-content -->
						</section><!-- .error-404 -->

					</div><!-- #main -->
				</section><!-- #primary -->

			</div><!-- .row -->
</div><!-- .container -->				

<?php
get_footer();
