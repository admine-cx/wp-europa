<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WP_Europa
 */

?>

	</div><!-- #content -->
    <?php //get_template_part( 'footer-widget' ); ?>
    <footer id="footer">
        <div class="footer-top-links">
            <a href="#">
                <h6><?php _e('CONTACT US', 'wp-europa') ?></h6>
            </a>
        </div>
        <div class="footer-top-links">
            <a href="#">
                <h6>FACEBOOK</h6>
            </a>
            <a href="#">
                <h6>LINKEDIN</h6>
            </a>
            <a href="#">
                <h6>INSTAGRAM</h6>
            </a>
        </div>
        <div class="row footer-border-top-bottom">
            <div class="col-12 col-md-6">
                <div class="footer-content footer-border-right">
                <?php 
                    $footer_style = types_render_field("select-footer", array('output'=>'raw','item'=>get_the_ID()));
                    if ($footer_style === "industrial-footer") {
                        dynamic_sidebar( 'footer-left-alt' );
                    } else {
                        dynamic_sidebar( 'footer-left' );
                    }
                    ?>
                    <!-- <div class="footer-content-inner">
                        <h4>
                            FIND A<br> 
                            PARTNER
                        </h4>
                        <p class="large-text">
                        Etiam porta sem malesuada magna mollis euismod. Curabitur blandit tempus porttitor. Maecenas sed diam eget risus varius blandit sit amet non magna.
                        </p>
                        <a href="#" class="btn-large btn--red">FIND YOUR PARTNER</a>
                        <a href="#" class="btn-large btn--black">CONTACT US</a>
                    </div> -->
                </div>
            </div>
            <div class="col-12 col-md-6">
                <div class="footer-content">
                    <?php  dynamic_sidebar( 'footer-right' ); ?>                    
                    <!-- <div class="footer-content-inner">
                        <h4>
                            STAY<br> 
                            IN TOUCH
                        </h4>
                        <form action="#">
                            <input type="text" placeholder="YOUR E-MAIL" name="email">
                            <input type="text" placeholder="YOUR NAME" name="name">
                            <input type="text" placeholder="YOUR TYPE" name="type"> 
                            <button type="submit" class="btn-large btn--black">OPT-IN</button>
                            <label class="d-flex align-items-center mt-4">
                                <input type="checkbox" name="checkbox" value="checked">
                                <div class="custom-checkbox"></div>
                                <span>YES I AGREE</span>
                            </label>
                        </form>
                        <div class="gdpr">
                            <h6>GDPR COMPILIANCE</h6>
                            <p>
                                BY SIGNING UP, YOU AGREE TO THE PRIVACY POLICY AND YOU WILL BE RECEIVING OUR NEWSLETTERS. YOU CAN WITHDRAW YOUR CONSENT BY CONTACTING US OR BY CLICKING ON THE “UNSUBSCRIBE” LINK AT THE END OF EACH NEWSLETTER.
                            </p>
                        </div>
                    </div> -->
                </div>
            </div>
        </div>
        <div class="footer-bottom-links">
            <div class="row">
                <div class="col-12 col-md-6 d-none d-md-block">
                    <?php wp_nav_menu( array(
                                'theme_location'    => "footer-left-menu",
                                'container_class'   => "footer-bottom-links-inner",
                            ) );?>  
                    <!-- <div class="footer-bottom-links-inner">
                        <ul>
                            <li>
                                <a href="#">OUR PRODUCTS</a>
                            </li>
                            <li>
                                <a href="#">OUR NETWORK</a>
                            </li>
                            <li>
                                <a href="#">CAREERS</a>
                            </li>
                            <li>
                                <a href="#">OUR PROJECTS</a>
                            </li>
                            <li>
                                <a href="#">ABOUT EUROPA</a>
                            </li>
                            <li>
                                <a href="#">PRESS</a>
                            </li>
                        </ul>
                    </div> -->
                </div>
                <div class="col-12 col-md-6">
                        <?php wp_nav_menu( array(
                                'theme_location'    => "footer-right-menu",
                                'container_class'   => "footer-bottom-links-inner footer-bottom-links-inner-2",
                            ) );?> 
                    <!-- <div class="footer-bottom-links-inner footer-bottom-links-inner-2">
                       <ul>
                            <li>
                                <a href="#">PRIVACY POLICY</a>
                            </li>
                            <li>
                                <a href="#">SITEMAP</a>
                            </li>
                            <li>
                                <a href="#">TERMS OF USE</a>
                            </li>
                        </ul> 
                    </div> -->
                </div>
            </div>
        </div>
</footer>

</div><!-- #page -->


<?php wp_footer(); ?>

</body>
</html>

