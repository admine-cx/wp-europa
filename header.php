<?php
/**
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 * @package WP_Europa
 */
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
<?php wp_head(); ?>
</head>
<?php 
    $europa_logo = '';
    if ( is_archive() ){
        $header_transparency = 'solid';
        $header_color = 'dark';
    }
    else if ( is_singular() ){
        $header_transparency = 'transparent';
        $header_color = 'light';
    }
    else if ( is_page() ){
        $header_transparency = 'transparent';
        $header_color = 'light';
    }
    else if ( is_search() ){
        $header_transparency = 'solid';
        $header_color = 'dark';
    }
    else if ( is_404() ){
        $header_transparency = 'solid';
        $header_color = 'dark';
    }
    else {
        $header_transparency = 'solid';
        $header_color = 'dark';
    }
   
    if ( (types_render_field("header-transparency", array('output'=>'raw','item'=>get_the_ID()))) ){
        $header_transparency = types_render_field("header-transparency", array('output'=>'raw','item'=>get_the_ID()));  
    }
    if( types_render_field("header-elements-color", array('output'=>'raw','item'=>get_the_ID())) ){
        $header_color = types_render_field("header-elements-color", array('output'=>'raw','item'=>get_the_ID()));
    }
    
    
    $header_extra_title = types_render_field("header-extra-page-title", array('item'=>get_the_ID()));   
  
   
    if ($header_transparency === 'solid'){
        $content_top_margin = 'content_top_margin';        
    }

    if (types_render_field("page-background-color", array('item'=>get_the_ID()))) {
        $page_bg_color = types_render_field("page-background-color", array('output'=>'raw','item'=>get_the_ID()));  
    }
    
?>
<body <?php body_class(); ?>>


<div class="custom-cursor">
    <div id="read-more">
        <span>READ MORE</span>
    </div>
    <div id="right-arrow">
        <svg id="arrow-right" viewBox="0 0 62 33">
            <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                <g id="FD.1.1.-Home-1920" transform="translate(-436.000000, -1746.000000)" stroke="#101010" stroke-width="2.4576">
                    <g id="Group" transform="translate(336.000000, 1747.000000)">
                        <path d="M144.334793,33.6594094 L145,34.322492 L134.933205,44.3571429 L134.267998,43.6940602 L144.334793,33.6594094 Z M116.665207,33.6594094 L126.732002,43.6940602 L126.066795,44.3571429 L116,34.322492 L116.665207,33.6594094 Z M130.885232,-13.6428571 L130.885232,29.9648662 L129.944488,29.9648662 L129.944488,-13.6428571 L130.885232,-13.6428571 Z" id="Combined-Shape-Copy-2" transform="translate(130.500000, 15.357143) scale(-1, 1) rotate(90.000000) translate(-130.500000, -15.357143) "></path>
                    </g>
                </g>
            </g>
        </svg>
        <span class="next-category"></span>
    </div>
</div>


<?php 

    // WordPress 5.2 wp_body_open implementation
    // if ( function_exists( 'wp_body_open' ) ) {
    //     wp_body_open();
    // } else {
    //     do_action( 'wp_body_open' );     }   
    
?>

<div id="page" class="site <?php echo $page_bg_color; ?>">
        <header class="<?php echo $header_transparency;?> <?php echo $header_color;?>">
                    <div class="row">
                        <div class="col-6">
                            <div class="row left-side">
                                <div class="col-4">
                                     <div  class="logo">
                                        <a href="/"><?php
                                        if (($header_transparency === 'transparent') && ( $header_color === 'light' )){ ?>
                                            <img id="logo-light" src="<?php echo content_url() . '/uploads/europa-logo.svg'?>" alt="Europa">
                                           <?php } else { ?>
                                            <img id="logo-dark" src="<?php echo content_url() . '/uploads/europa-logo-dark.svg'?>" alt="Europa">
                                          <?php  } ?>    
                                           <img id="logo-scroll" src="<?php echo content_url() . '/uploads/europa-logo-dark.svg'?>" alt="Europa">                                  
                                            
                                        </a>
                                    </div>                                   
                                </div>
                                <div class="col-8">
                                <div class="small-page-title">
                                    <h5><?php echo $header_extra_title;?></h5>                                                    
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6 d-flex">
                            <div class="right-side">
                                <!-- <div class="language-switcher">    -->
                                    <?php wp_nav_menu( array(
                                            'theme_location'    => "language-menu",
                                            'container_class'   => "language-switcher",
                                        ) );?>                                 
                                    <!-- <span class="active-language">
                                        <a href="#">EN</a>
                                    </span>
                                    <span>
                                        <a href="#">GR</a>
                                    </span> -->
                                <!-- </div> -->
                                <div class="consumer-line">
                                    <h6><?php _e('CONSUMER DIRECT LINE', 'wp-europa'); ?></h6>
                                    <span class="cosnumer-line-tel">+(30) 22620-32100</span>                    
                                </div>
                                <div class="nav-button">
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </header><!-- #masthead -->

	<div id="content" class="site-content <?php echo $content_top_margin;?>">



           



