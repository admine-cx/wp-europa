<?php /* Template Name: Landings Template */ ?>

<?php get_header(); ?>


<!-- Top section - Landing Pages -->

<?php get_template_part('modules/landing-top-section'); ?> 


<div class="container">
			<div class="row">
                <section id="primary" class="content-area col-sm-12">
                        <div id="main" class="site-main" role="main">

                            <?php
                            while ( have_posts() ) : the_post();

                                get_template_part( 'template-parts/content', 'page' );
                        
                            endwhile; // End of the loop.
                            ?>

                        </div><!-- #main -->
                </section><!-- #primary -->

    </div><!-- .row -->
</div><!-- .container -->





<?php get_footer(); ?>
