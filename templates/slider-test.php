<?php /* Template Name: Slider Test */ ?>

<?php get_header(); ?>

<div class="spacer"></div>





<div class="swiper-container posts-slider" id="posts-slider">
    <div class="posts-slider-navigation">
        <ul>
            <li>EXHIBITIONS</li>
            <li>CORPORATE</li>
            <li>CSR</li>
            <li>NEWS dfsdfs</li>
            <li>EVENTS</li>
            <li>ΑΚΟΜΑΕΝΑ</li>
        </ul>
    </div>
    <div class="swiper-wrapper">
        <div class="swiper-slide">
            <div class="bg-image-wrapper">
                <div class="bg-image-inner" style="background-image: url('https://source.unsplash.com/1601x900/?abstract')"></div>
            </div>
            <div class="box-container">
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
            </div>
           
        </div>
        <div class="swiper-slide">
        <div class="bg-image-wrapper">
                <div class="bg-image-inner" style="background-image: url('https://source.unsplash.com/1601x900/?abstract')"></div>
            </div>
            <div class="box-container">
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
        <div class="bg-image-wrapper">
                <div class="bg-image-inner" style="background-image: url('https://source.unsplash.com/1601x900/?abstract')"></div>
            </div>
            <div class="box-container">
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>              
            </div>
        </div>
        <div class="swiper-slide">
        <div class="bg-image-wrapper">
                <div class="bg-image-inner" style="background-image: url('https://source.unsplash.com/1601x900/?abstract')"></div>
            </div>
            <div class="box-container">
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
            </div>
        </div>
        <div class="swiper-slide">
        <div class="bg-image-wrapper">
                <div class="bg-image-inner" style="background-image: url('https://source.unsplash.com/1601x900/?abstract')"></div>
            </div>
            <div class="box-container">
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
               
            </div>
        </div>
        <div class="swiper-slide">
        <div class="bg-image-wrapper">
                <div class="bg-image-inner" style="background-image: url('https://source.unsplash.com/1601x900/?abstract')"></div>
            </div>
            <div class="box-container">
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
                <div class="box-info">
                    <h5 class="box-info-title">EVENT TITLE</h5>
                    <span>LOCATION</span>
                    <div class="box-info-date">20-12 2020</div>
                    <a href="#" class="btn-small btn--red">SEE MORE</a>
                </div>
            </div>
        </div>
    </div>

    <div class="swiper-button-next" data-custom-cursor="right-arrow"></div>

</div>








<div class="spacer"></div>



<?php get_footer(); ?>

