<?php /* Template Name: Home Template */ ?>

<?php get_header(); ?>


<?php get_template_part('modules/home-hero-section'); ?> 

<!-- Fancy Box - Story Box -->

<!-- <div class="row">
    <div class="col-12 col-lg-4">
        <?php get_template_part('modules/fancy-box-stories'); ?> 
    </div>
    <div class="col-12 col-lg-4">
        <?php get_template_part('modules/fancy-box-stories'); ?> 
    </div>
    <div class="col-12 col-lg-4">
        <?php get_template_part('modules/fancy-box-stories'); ?> 
    </div>
</div> -->



<!-- Fancy Box - Small Image -->

<!-- <div class="container">
    <div class="row">
        <div class="col-12 col-md-6">
            <?php get_template_part('modules/fancy-box-small-image'); ?> 
        </div>
        <div class="col-12 col-md-6">
            <?php get_template_part('modules/fancy-box-small-image'); ?> 
        </div>
        <div class="col-12 col-md-6">
            <?php get_template_part('modules/fancy-box-small-image'); ?> 
        </div>
        <div class="col-12 col-md-6">
            <?php get_template_part('modules/fancy-box-small-image'); ?> 
        </div>
   </div>
</div> -->

<div class="container">
			<div class="row">
                <section id="primary" class="content-area col-sm-12">
                        <div id="main" class="site-main" role="main">

                            <?php
                            while ( have_posts() ) : the_post();

                                get_template_part( 'template-parts/content', 'page' );
                        
                            endwhile; // End of the loop.
                            ?>

                        </div><!-- #main -->
                </section><!-- #primary -->

    </div><!-- .row -->
</div><!-- .container -->

<!-- <div class="row">
    <div class="col-12 col-md-6">
        <?php get_template_part('modules/fancy-box-bg-image-hover'); ?> 
    </div>
    <div class="col-12 col-md-6">
        <?php get_template_part('modules/fancy-box-bg-image-hover'); ?> 
    </div>
</div> -->

<!-- Fancy Box - Background image on hover -->
<!-- 
<div class="row">
    <div class="col-12 col-md-6">
        <?php get_template_part('modules/fancy-box-bg-vector'); ?> 
    </div>
    <div class="col-12 col-md-6">
        <?php get_template_part('modules/fancy-box-bg-vector'); ?> 
    </div>
</div> -->







<!-- placeholder size sto footer!!! -->

<?php get_footer(); ?>
