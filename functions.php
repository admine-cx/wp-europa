<?php
/**
 * WP_Europa functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package WP_Europa
 */

if ( ! function_exists( 'wp_europa_setup' ) ) :
/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function wp_europa_setup() {
	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 * If you're building a theme based on WP Bootstrap Starter, use a find and replace
	 * to change 'wp-europa' to the name of your theme in all the template files.
	 */
	load_theme_textdomain( 'wp-europa', get_template_directory() . '/languages' );

	// Add default posts and comments RSS feed links to head.
	add_theme_support( 'automatic-feed-links' );

	/*
	 * Let WordPress manage the document title.
	 * By adding theme support, we declare that this theme does not use a
	 * hard-coded <title> tag in the document head, and expect WordPress to
	 * provide it for us.
	 */
	add_theme_support( 'title-tag' );

	/*
	 * Enable support for Post Thumbnails on posts and pages.
	 *
	 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
	 */
	add_theme_support( 'post-thumbnails' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => esc_html__( 'Primary', 'wp-europa' ),
	) );

	/*
	 * Switch default core markup for search form, comment form, and comments
	 * to output valid HTML5.
	 */
	add_theme_support( 'html5', array(
		'comment-form',
		'comment-list',
		'caption',
	) );

	// Set up the WordPress core custom background feature.
	add_theme_support( 'custom-background', apply_filters( 'wp_europa_custom_background_args', array(
		'default-color' => 'ffffff',
		'default-image' => '',
	) ) );

	// Add theme support for selective refresh for widgets.
	add_theme_support( 'customize-selective-refresh-widgets' );

    function wp_boostrap_starter_add_editor_styles() {
        add_editor_style( 'custom-editor-style.css' );
    }
    add_action( 'admin_init', 'wp_boostrap_starter_add_editor_styles' );

}
endif;
add_action( 'after_setup_theme', 'wp_europa_setup' );


/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function wp_europa_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'wp_europa_content_width', 1440 );
}
add_action( 'after_setup_theme', 'wp_europa_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function wp_europa_widgets_init() {
    register_sidebar( array(
        'name'          => esc_html__( 'Sidebar', 'wp-europa' ),
        'id'            => 'sidebar-1',
        'description'   => esc_html__( 'Add widgets here.', 'wp-europa' ),
        'before_widget' => '<section id="%1$s" class="widget %2$s">',
        'after_widget'  => '</section>',
        'before_title'  => '<h3 class="widget-title">',
        'after_title'   => '</h3>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer left Column', 'wp-europa' ),
        'id'            => 'footer-left',
        'description'   => esc_html__( 'Add widgets here.', 'wp-europa' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s footer-content-inner">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer right Column', 'wp-europa' ),
        'id'            => 'footer-right',
        'description'   => esc_html__( 'Add widgets here.', 'wp-europa' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s footer-content-inner">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );
    register_sidebar( array(
        'name'          => esc_html__( 'Footer left Alternative', 'wp-europa' ),
        'id'            => 'footer-left-alt',
        'description'   => esc_html__( 'Add widgets here.', 'wp-europa' ),
        'before_widget' => '<div id="%1$s" class="widget %2$s footer-content-inner">',
        'after_widget'  => '</div>',
        'before_title'  => '<h4 class="widget-title">',
        'after_title'   => '</h4>',
    ) );
}
add_action( 'widgets_init', 'wp_europa_widgets_init' );


/**
 * Enqueue scripts and styles.
 */
function wp_europa_scripts() {


    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/assets/css/bootstrap.min.css' );
    wp_enqueue_style('swiper', get_template_directory_uri() . '/assets/css/swiper.min.css' );

    wp_enqueue_style('style', get_template_directory_uri() . '/assets/css/style.css', '999');

    wp_enqueue_style('google-font-mono', 'https://fonts.googleapis.com/css2?family=Roboto+Mono:wght@400;700&display=swap' );     
    wp_enqueue_style('google-font-condensed', 'https://fonts.googleapis.com/css2?family=Roboto+Condensed:wght@300;400;700&display=swap' );   


    wp_enqueue_script('gsap', 'https://cdnjs.cloudflare.com/ajax/libs/gsap/3.5.1/gsap.min.js');
    wp_enqueue_script('jquery-latest', 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js');
    wp_enqueue_script('swiper', get_template_directory_uri() . '/assets/js/swiper.min.js');
    wp_enqueue_script('main', get_template_directory_uri() . '/assets/js/main.js');  


}
add_action( 'wp_enqueue_scripts', 'wp_europa_scripts' );




function wp_europa_password_form() {
    global $post;
    $label = 'pwbox-'.( empty( $post->ID ) ? rand() : $post->ID );
    $o = '<form action="' . esc_url( home_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" method="post">
    <div class="d-block mb-3">' . __( "To view this protected post, enter the password below:", "wp-europa" ) . '</div>
    <div class="form-group form-inline"><label for="' . $label . '" class="mr-2">' . __( "Password:", "wp-europa" ) . ' </label><input name="post_password" id="' . $label . '" type="password" size="20" maxlength="20" class="form-control mr-2" /> <input type="submit" name="Submit" value="' . esc_attr__( "Submit", "wp-europa" ) . '" class="btn btn-primary"/></div>
    </form>';
    return $o;
}
add_filter( 'the_password_form', 'wp_europa_password_form' );


/**
 * Implement the Custom Header feature.
 */
// require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Custom functions that act independently of the theme templates.
 */
require get_template_directory() . '/inc/extras.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';


/**
 * Load custom WordPress nav walker.
 */
if ( ! class_exists( 'wp_bootstrap_navwalker' )) {
    require_once(get_template_directory() . '/inc/wp_bootstrap_navwalker.php');
}

function wpb_custom_menu_locations() {
    register_nav_menus(
      array(
        'language-menu' => __( 'Language Menu' ),
        'mobile-menu' => __( 'Moblie Menu' ),
        'footer-left-menu' => __( 'Footer Left Menu' ),
        'footer-right-menu' => __( 'Footer Right Menu' )
      )
    );
  }
  add_action( 'init', 'wpb_custom_menu_locations' );

  


// if(function_exists('vc_add_shortcode_param')) {
// 	// echo '<script>alert("Welcome to Geeks for Geeks")</script>'; 
// 	/**
// 	 * Create multi dropdown param type.
// 	 *
// 	 * @since 1.0
// 	 */
// 	// vc_add_shortcode_param( 'dropdown_multi', 'dropdown_multi_settings_field', plugin_dir_url( __FILE__ ).'/js/backend-edit-form-bulk.js');
// 	// function dropdown_multi_settings_field( $param, $value ) {

// 	// 	 $param_line = '';
// 	// 	 $param_line .= '<select multiple name="'. esc_attr( $param['param_name'] ).'" class="wpb_vc_param_value wpb-input wpb-select '. esc_attr( $param['param_name'] ).' '. esc_attr($param['type']).'">';
// 	// 	foreach ( $param['value'] as $text_val => $val ) {
// 	// 		if ( is_numeric($text_val) && (is_string($val) || is_numeric($val)) ) {
// 	// 			$text_val = $val;
// 	// 		}

// 	// 		$selected = '';

// 	// 		if(!is_array($value)) {
// 	// 			$param_value_arr = explode(',',$value);
// 	// 		} else {
// 	// 			$param_value_arr = $value;
// 	// 		}
	
// 	// 		if ($value!=='' && in_array($val, $param_value_arr)) {
// 	// 			$selected = ' selected="selected"';
// 	// 		}
// 	// 		$param_line .= '<option class="'.$val.'" value="'.$val.'"'.$selected.'>'.$text_val.'</option>';
// 	// 	}
// 	// 	$param_line .= '</select>';

// 	//    return  $param_line;
// 	// }
// vc_add_shortcode_param( 'dropdown_multi', 'dropdown_multi_settings_field' );

// function dropdown_multi_settings_field( $param, $value ) {

//     $param_line = '';

//     $param_line .= '<select multiple name="'. esc_attr( $param['param_name'] ).'" class="wpb_vc_param_value wpb-input wpb-select '. esc_attr( $param['param_name'] ).' '. esc_attr($param['type']).'">';

//     foreach ( $param['value'] as $text_val => $val ) {

//         if ( is_numeric($text_val) && (is_string($val) || is_numeric($val)) ) {

//             $text_val = $val;

//         }

//         $text_val = __($text_val, "js_composer");

//         $selected = '';

//         if(!is_array($value)) {

//             $param_value_arr = explode(',',$value);

//         } else {

//             $param_value_arr = $value;

//         }

//         if ($value!=='' && in_array($val, $param_value_arr)) {

//             $selected = ' selected="selected"';

//         }

//         $param_line .= '<option class="'.$val.'" value="'.$val.'"'.$selected.'>'.$text_val.'</option>';

//     }

//     $param_line .= '</select>';

//     return  $param_line;

// }
	
// }