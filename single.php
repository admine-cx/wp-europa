<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package WP_Europa
 */


get_header(); ?>

<div class="container">
			<div class="row">
                <section id="primary" class="content-area col-sm-12">
                        <div id="main" class="site-main" role="main">

                            <?php
                            while ( have_posts() ) : the_post();

                                get_template_part( 'template-parts/content', '' );
                        
                            endwhile; // End of the loop.
                            ?>

                        </div><!-- #main -->
                </section><!-- #primary -->

    </div><!-- .row -->
</div><!-- .container -->

<!-- placeholder size sto footer!!! -->

<?php get_footer(); ?>
