window.onload = function () {

    // Header sticky and solid on scoll
    // Να το βελτιώσουμε αυτό. Όταν κάνουμε refresh τη σελίδα να είναι πάλι sticky and solid
    fixedHeaderOnScroll();

    // if the page has the home slider
    if(document.querySelector('.home-hero-slider')){
        console.log('home hero slider is here');
        homeSlider();
    }
    postsSlider();
    initCustomCursor();
}


function homeSlider() {
    
    var sliderImages = document.querySelectorAll('.home-hero-slider .slide-background');
    var sliderTitles = document.querySelectorAll('.home-hero-slider .slide-title');
    var sliderDescription = document.querySelectorAll('.home-hero-slider .slide-description');
    var sliderDateInfo = document.querySelectorAll('.home-hero-slider .slide-date-info');
    var counter = 0;
    var slidesLength = document.querySelectorAll('.home-hero-slider .slide').length;

    //Initial Slide
    gsap.to(sliderImages[0], {
        duration: 0,
        autoAlpha: 1
    })
    gsap.to(sliderTitles[0], {
        duration: 0,
        autoAlpha: 1
    })
    gsap.to(sliderDescription[0], {
        duration: 0,
        autoAlpha: 1
    })
    gsap.to(sliderDateInfo[0], {
        duration: 0,
        autoAlpha: 1
    })

    function changeSlide() {

        function slideIn(slideIndex){
            gsap.to(sliderImages[slideIndex], {
                duration: 0.6,
                delay: 0.2,
                autoAlpha: 1
            })
            gsap.fromTo(sliderTitles[slideIndex], {
                y: 30,
                autoAlpha: 0
            },{
                duration: 0.6,
                y: 0,
                delay: 1,
                autoAlpha: 1,
                ease: 'power2.out'
            })
            gsap.fromTo(sliderDescription[slideIndex], {
                y: 30,
                autoAlpha: 0
            },{
                duration: 0.6,
                y: 0,
                delay: 1.15,
                autoAlpha: 1,
                ease: 'power2.out'
            })
            gsap.fromTo(sliderDateInfo[slideIndex], {
                y: 25,
                autoAlpha: 0
            },{
                duration: 0.55,
                y: 0,
                delay: 1.2,
                autoAlpha: 1,
                ease: 'power2.out'
            })
        }

        function slideOut(slideIndex) {
            gsap.to(sliderImages[slideIndex], {
                duration: 0.6,
                delay: 0.6,
                autoAlpha: 0
            })
            gsap.to(sliderTitles[slideIndex], {
                duration: 0.6,
                y: -30,
                autoAlpha: 0,
                ease: 'power2.in'
            })
            gsap.to(sliderDescription[slideIndex], {
                duration: 0.6,
                y: -30,
                delay: 0.15,
                autoAlpha: 0,
                ease: 'power2.in'
            })
            gsap.to(sliderDateInfo[slideIndex], {
                duration: 0.55,
                y: -25,
                delay: 0.2,
                autoAlpha: 0,
                ease: 'power2.in'
            })
        }
        
        if (counter == slidesLength - 1) {
            slideIn(0);
            slideOut(slidesLength - 1);
            counter = 0;
        }
        else{
            slideIn(counter + 1);
            slideOut(counter)
            counter++;
        }

        //Animate cursor
        
    }



    var sliderInterval = setInterval(changeSlide, 8000);
  
    // This is for setInterval buffering problem
    window.addEventListener('focus', function () {
        sliderInterval = setInterval(changeSlide, 8000);
    });   
    window.addEventListener('blur', function () {
        clearInterval(sliderInterval);
    });



}



function fixedHeaderOnScroll() {

    var header = document.querySelector('header');
    var logoScroll = document.getElementById('logo-scroll');

    window.addEventListener('scroll', function () {
        if (window.scrollY > 100) {
            header.classList.add('fixed-header');
            logoScroll.classList.add('logo-visible');
        }
        else{
            header.classList.remove('fixed-header');
            logoScroll.classList.remove('logo-visible');
        }
    })
}


//mySwiper.activeIndex



function postsSlider() {

    var swiper = new Swiper('#posts-slider',{
        slidesPerView: 1,
        loop: true,
        speed: 1100,
        allowTouchMove: false,
        navigation: {
            nextEl: '.swiper-button-next',
        },
        breakpoints: {
            1000: {
                slidesPerView: 1.2
            }
        }
    });

    var navigationMenu = document.querySelectorAll('.posts-slider-navigation ul');
    var navigationLinks = document.querySelectorAll('.posts-slider-navigation ul li');
    var nextButton = document.querySelector('.swiper-button-next');


    navigationLinks[0].classList.add('active');
    
    navigationLinks.forEach(function (navigationLink, index) {

        navigationLink.addEventListener('click', function (e) {

            navigationLinks.forEach(function (link) {
                link.classList.remove('active');
            })
            navigationLink.classList.add('active');
            swiper.slideToLoop(index, 1100, false);
            gsap.to(navigationMenu, {
                duration: 0.5,
                x: -e.target.offsetLeft
            })

        })

    })

    swiper.on('slideChangeTransitionStart', function () {

        var activeIndex = document.querySelector('.swiper-slide-active').dataset.swiperSlideIndex;
        navigationLinks.forEach(function (link) {
            link.classList.remove('active');
        })
        navigationLinks[activeIndex].classList.add('active');
        gsap.to(navigationMenu, {
            duration: 0.5,
            x: -navigationLinks[activeIndex].offsetLeft
        })


    });

    nextButton.addEventListener('click', function () {
        gsap.to('.custom-cursor', {
            keyframes: [
                { scale: 0, opacity: 0, duration: 0.24, ease: 'power2.in'},
                { scale: 1, opacity: 1, duration: 0.34, ease: 'power2.out', duration: 0.2}
            ]
        })
    })
    

}


function initCustomCursor() {

    var mousePos = {};

    window.addEventListener('mousemove', function (e) {
        mousePos.x = e.clientX;
        mousePos.y = e.clientY;
    })

    var customCursorModules = document.querySelectorAll('[data-custom-cursor]');
    var customCursor = document.querySelector('.custom-cursor');

    customCursorModules.forEach(function (module) {
        module.addEventListener('mouseenter', function (e) {

            var customCursorValue = e.target.dataset.customCursor;

            module.style.cursor = 'none';

            switch (customCursorValue) {
                case 'read-more':
                    gsap.fromTo('.custom-cursor #read-more', {
                        scale: 0,
                        opacity: 0
                    },{
                        scale: 1,
                        opacity: 1,
                        duration: 0.5
                    })
                    break;
                case 'right-arrow':
                    gsap.fromTo('.custom-cursor #right-arrow', {
                        scale: 0,
                        opacity: 0
                    },{
                        scale: 1,
                        opacity: 1,
                        duration: 0.5
                    })
                    break;
                default:
                    break;
            }
        })

        module.addEventListener('mouseleave', function (e) {

            module.style.cursor = 'auto';

            gsap.to('.custom-cursor > div', {
                scale: 0,
                opacity: 0
            })
        })
    })

    function moveMouse() {
        gsap.to('.custom-cursor', {
            duration: 0.4,
            x: mousePos.x,
            y: mousePos.y
        })

        requestAnimationFrame(moveMouse);
    }

    requestAnimationFrame(moveMouse);
    
}